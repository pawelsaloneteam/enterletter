package ak.bootcamp;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Font;

public class EnterLetterController {
    @FXML
    private Label labelRecommendation;
    @FXML
    private Label labelLetter;

    SentenceManager sentenceManager;


     public void handleKeyPressed(KeyEvent key) {
         String letter = key.getCode().getChar().toLowerCase();
        if(letter.equals(sentenceManager.getCurrentLetter().toLowerCase())){
            if(sentenceManager.hasNextLetter()){
                labelLetter.setText(sentenceManager.getNextLetter());
            }else{
                labelRecommendation.setText(sentenceManager.getSentence());
                labelLetter.setFont(new Font(12.0));
                labelLetter.setText(sentenceManager.printWrongKeysPressed());
            }
        }else{
            sentenceManager.addWrongKeyPressed(letter);
        }
    }

    @FXML
    public void initialize() {
        sentenceManager = new SentenceManager();
        if(sentenceManager.hasNextLetter()){
            labelLetter.setFont(new Font(48.0));
            labelLetter.setText(sentenceManager.getNextLetter());
            labelRecommendation.setText("Press key with letter shown below");
        }else{
            labelLetter.setText("");
            labelRecommendation.setText("There is no sentence.");
        }
    }
}
