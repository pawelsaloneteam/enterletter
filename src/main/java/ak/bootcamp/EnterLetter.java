package ak.bootcamp;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class EnterLetter extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getClassLoader().getResource("EnterLetter.fxml"));
        Parent root = loader.load();

        EnterLetterController controller = loader.getController();
        Scene scene = new Scene(root);
        scene.addEventHandler(KeyEvent.KEY_PRESSED, (key) -> {
            controller.handleKeyPressed(key);
        });
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}
