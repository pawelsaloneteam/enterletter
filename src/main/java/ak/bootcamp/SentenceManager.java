package ak.bootcamp;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;

public class SentenceManager {
    private String sentence;
    private String sentenceWithoutBlankSpaces;
    private int index;
    private Multiset<String> wrongKeyPressedCounter;
    private String currentLetter;

    public SentenceManager(){
        sentence="Ala ma kota.";
        sentenceWithoutBlankSpaces = sentence.replaceAll(" ", "");
        index=0;
        wrongKeyPressedCounter = HashMultiset.create();
    }

    public String getSentence() {
        return sentence;
    }

    public String getNextLetter(){
        String nextLetter =  sentenceWithoutBlankSpaces.substring(index, index+1);
        index++;
        currentLetter = nextLetter;
        return nextLetter;
    }

    public void addWrongKeyPressed(String letter){
            wrongKeyPressedCounter.add(letter);
      }

    public String printWrongKeysPressed(){
        String result = "Wrong keys pressed occurences:\n";
        for(String letter : wrongKeyPressedCounter.elementSet()){
            result += letter+": "+wrongKeyPressedCounter.count(letter)+"\n";
        }

        return result;
    }

    public String getCurrentLetter() {
        return currentLetter;
    }

    public boolean hasNextLetter() {
        return index < sentenceWithoutBlankSpaces.length() ? true : false;
    }
}
