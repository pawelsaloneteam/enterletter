Program przedstawia rozwiązanie zadania zadanego w ramach kursu Java.

Stworzyć uruchamialną aplikację okienkową, która wyświetla jedną literę i czeka na użytkownika
dopóki nie wciśnie tej właśnie litery.
Po poprawnym wciśnięciu litery wyświetlana jest następna litera.
Wyświetlane litery tworzą zdanie, na przykład „Andrzej jest najlepszy”. Po wyczerpaniu się liter
wyświetlana jest liczba pomyłek jakie użytkownik popełnił dla każdej litery.
